## Diagnostic Panels API


### Cpu Thermal Zones
Request the current temperature readouts for each thermal zone.  Each call returns a single instance of Thermal readings.
```
POST server/cpu_thermal_zones
```

Resolution response
```json
POST server/cpu_thermal_zones

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response
```json

{"success":true,"results":{"battery":18.8}}

```
There may be more than a single value, but each value will represent a single named thermal zone that is measured during normal device operation.  Vaues represents the temperature in degrees C.

### Cpu Load average
Request the load averages maintained by the kernel.  Averages are kept as 1 minute, 5 minute and 15 minute average values.  Each call returns a single set of averages.
```
POST server/load_avg
```

Resolution response
```json
POST server/load_avg

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response
```json
{"success":true,"results":{"1_min_avg":458,"5_min_avg":397,"15_min_avg":282}}

```
While the raw load average is expressed as a floating point value of the overall processor load, the returned value is an integer value representing the (%load/#cores) * 100 , eg 1.03 will be returned as 103.

### IO Stats
Request disk read/write performance statistics.
```
POST server/io_stats
```

Resolution response
```json
POST server/load_avg

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response
```json
{
	"success": true,
	"results": {
		"data": [{
			"device": "rknand_cache",
			"reads": "22",
			"writes": "17",
			"in_progress": "0",
			"time_in_io": "50"
		}, {
			"device": "rknand_metadata",
			"reads": "18",
			"writes": "7",
			"in_progress": "0",
			"time_in_io": "30"
		}, {
			"device": "rknand_system",
			"reads": "3756",
			"writes": "2",
			"in_progress": "0",
			"time_in_io": "17900"
		}, {
			"device": "rknand_userdata",
			"reads": "1921",
			"writes": "5483",
			"in_progress": "0",
			"time_in_io": "14500"
		}, {
			"device": "dm-0",
			"reads": "2752",
			"writes": "5969",
			"in_progress": "0",
			"time_in_io": "20650"
		}]
	}
}
```


### Ping tests
Returns the results of a set of pings issued on the device to measure the connectivity quality.  The ip address resolved and the round-trip time are returned for each host that was pinged.
```
POST server/ping_hosts
```

Resolution response
```json
POST server/ping_hosts

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response
```json
{
	"success": true,
	"results": {
		"data": [{
			"host": "google.com (172.217.5.78)",
			"ping": "11.452"
		}, {
			"host": "yahoo.com (98.137.246.8)",
			"ping": "38.815"
		}, {
			"host": "twitter.com (104.244.42.1)",
			"ping": "24.538"
		}, {
			"host": "www.buzztime.com (216.32.152.6)",
			"ping": "14.298"
		}, {
			"host": "services.dev.buzztime.com (216.32.152.73)",
			"ping": "10.796"
		}, {
			"host": "ntnservices.dev.buzztime.com (216.32.152.74)"
		}, {
			"host": "rabbitmq.dev.buzztime.com (216.32.152.75)"
		}]
	}
}
```

### Bandwidth report
Request the results of /proc/net/dev call. 

|name | value
 ---- | -------
kbyte | The total number of bytes/1024 of data transmitted or received by the interface.
pkt | The total number of packets of data transmitted or received by the interface.
err | The total number of transmit or receive errors detected by the device driver.
drp | The total number of packets dropped by the device driver.
cmpr | The number of compressed packets transmitted or received by the device driver. (This appears to be unused in the 2.2.15 kernel.)
mc | The number of multicast frames transmitted or received by the device driver.

```
POST server/bandwidth
```

Resolution response
```json
POST server/bandwidth

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response

```json
POST server/bandwidth

{
	"success": true,
	"results": {
		"data": [{
			"Iface": "wlan0 rx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "wlan0 tx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "p2p0 rx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "p2p0 tx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "sit0 rx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "sit0 tx",
			"kbyte": 0,
			"pkt": "0",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "lo rx",
			"kbyte": 710,
			"pkt": "14542",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "lo tx",
			"kbyte": 710,
			"pkt": "14542",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "eth0 rx",
			"kbyte": 264360,
			"pkt": "310073",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}, {
			"Iface": "eth0 tx",
			"kbyte": 6903,
			"pkt": "80085",
			"err": "0",
			"drp": "0",
			"cmpr": "0",
			"mc": "0"
		}]
	}
}

```

### List Usb Devices
List USB devices that are present on the USB 2.0 hub.  The empty Audio devices are filtered out to reduce clutter in the list.
```
POST server/list_usbdevices
```

Resolution response
```json
POST server/list_usbdevices

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

Example response
```json
POST server/list_usbdevices

{
	"success": true,
	"results": {
		"data": [{
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 0,
			"iName": "Realtek USB2.0 Audio",
			"iClass": "Audio",
			"iSub": 1,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 1,
			"iName": "Headphone",
			"iClass": "Audio",
			"iSub": 2,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 2,
			"iName": "Microphone",
			"iClass": "Audio",
			"iSub": 2,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 3,
			"iName": "Headphone",
			"iClass": "Audio",
			"iSub": 2,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 4,
			"iName": "SPDIF",
			"iClass": "Audio",
			"iSub": 2,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 5,
			"iName": "Headphone",
			"iClass": "Audio",
			"iSub": 2,
			"Prot": 0
		}, {
			"ID": 2002,
			"Name": "\/dev\/bus\/usb\/002\/002",
			"Class": 0,
			"vnd_ID": 3034,
			"prd_ID": 18458,
			"iID": 255,
			"iClass": "HID",
			"iSub": 0,
			"Prot": 0
		}, {
			"ID": 1003,
			"Name": "\/dev\/bus\/usb\/001\/003",
			"Class": 0,
			"vnd_ID": 1008,
			"prd_ID": 54279,
			"iID": 0,
			"iClass": "HID",
			"iSub": 1,
			"Prot": 1
		}, {
			"ID": 1003,
			"Name": "\/dev\/bus\/usb\/001\/003",
			"Class": 0,
			"vnd_ID": 1008,
			"prd_ID": 54279,
			"iID": 1,
			"iClass": "HID",
			"iSub": 1,
			"Prot": 2
		}]
	}
}
```


