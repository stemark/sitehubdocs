
## Sitehub Dashboard API

* [System Status](system-status.md)
* [Content](content.md)
    * [Package Logtags](package-logs.md)
    * [System Logs](site-logs.md)
    * [System Updates](system-update.md)
* [Diagnostic](diagnostic.md)
* [Profiling](profiling.md)

### Introduction
Sitehubs are typically found behind firewalls of varying degrees of strictness.  Firewalls are not normally controlled by buzztime and the likelihood is that most sites will not be allowed to receive unsolicited tcp requests from the wild internet.

The model adopted for sitehubs relies on each hub initiating a connection to our corporate internet.  RabbitMQ, configured with MQTT is the backend host for those connections. This allows the sitehub clients to use the lightweight MQTT framework.

On Sitehub Startup:
1. Boot sequence
1. Init Services
1. Connect to RabbitMQ
   *  Subscribe to site/_siteid_

Once the site is connected, messages can be published to the sitehub topic, with a replyto address.

The format for messages published consists of a replyto line, followed by a GET or POST with an action and an optional receiver category.

### replyto line
The replyto defines either an MQTT topic for response data.  Since multiple browser sessions may be inquiring of a sitehub, the replyto allows the receivers to be differentiated.  Each session requesting data should create a replyto topic that will be unique among all sessions.

```
replyto session/Ka0HOEDYRiwe4rbx
```
Alternatively, the replyto object may use an http endpoint, but details would need to be negotiated.

### Action line
The action line starts with either POST or GET and is followed by the target info
* **GET** is used to make a direct query against a Content provider running on the system.
* **POST** is used to send a message to a service running on the system.
The target may optionally limit the scope using a category, followed by the action to perform.  `POST server/init_log` will send an intent to the server to retrieve the core app's init log and deliver it to the replyto target.

### data block
Each message can optionally contain a data block that can be used to customize the action(s) performed.  The data block is separated from the replyto and POST lines with a blank line and always consists of json data.

### Examples
Requesting the init_logs
```
replyto session/Ka0HOEDYRiwe4rbx
POST server/init_logs
```
Requests the init logs to be published to the MQTT topic 'session/Ka0HOEDYRiwe4rbx'.  This request doesn't require any additional parameters to the data block is not present.

Reboot the WAP:
```
replyto session/Ka0HOEDYRiwe4rbx
POST ruckus/reboot

{ 'reboot': { 'now': 0 } }
```
Direct the sitehub to send a reboot command to a connected ruckus router.
The data block passes config info on how long to instruct the router to wait before initiating the reboot operation.

## Return data
Two messages are returned for a request.  The first message acknowledges receipt and resolves the content provider or service that will receive the message.

### Request Acknowledgement & Resolution
```
POST server/init_logs

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```
The POST line refers to the request that was received.  The MQTT replyto topic is contained in the MQTT message object, so there is no need to include it in the message body.

The data block identifies the Service that will receive the message. There is no data included at this point because the message has only just been delivered to the service.

The type can be a content provider, a service, or if nothing was resolved: a broadcast.  While there may be times when a broadcast is desired, it should generally be seen as a failure to deliver the message to it's intended target.

### Response
The second message is structured identically, except for the data block.  In this message the data block contains the response status and resulting data.

```
POST server/init_logs

{"success":true,"results":{"data":[{"tag":"CalibrateClock","status":"checking","result":"","log":""},{"tag":"CoreInstaller","status":"done","result":"posted","log":""},{"tag":"GetInstalledPackagesTask","status":"done","result":"17 packages","log":""},{"tag":"Nightly reboot scheduled","status":"done","result":"03:00","log":""},{"tag":"PackageSystemCheckTask","status":"done","result":"GOOD","log":""},{"tag":"Request:SiteHandshake","status":"done","result":"NTN Site Hub - Test Site #3","log":""},{"tag":"Request:SoftwareManifest","status":"done","result":"sitehubTest","log":""},{"tag":"SiteHandshakeUpdateTask","status":"created","result":"","log":""},{"tag":"SiteManifestKeys","status":"created","result":"","log":""},{"tag":"SiteSoftwareDepCheckTask","status":"done","result":"Valid","log":""},{"tag":"SiteSoftwareDownloadTask","status":"done","result":"up-to-date","log":""},{"tag":"SiteStatusReporting","status":"done","result":"posted","log":""},{"tag":"SoftwareDownloadMonitor","status":"done","result":"success","log":""},{"tag":"SoftwareInstaller","status":"done","result":"success","log":""},{"tag":"handoff","status":"done","result":"com.buzztime.sched.Supervisor\/com.buzztime.supervisor.SupervisorActivity","log":""},{"tag":"pkg:CoreApp-mastermind-dev-debug-132-1.6.0-SNAPSHOT.apk","status":"update","result":"see notification","log":""}]}}
```
The result block contains the status and resulting data returned from the target.
In this case the resulting data is an array of rows listing the results of the morning's reboot initialization.

Data will always be returned as json data, but the data member may contain an array or an object.



## Sites API

The Sites tab is a portal to Sitehub devices that are active on RabbitMQ.

### MQTT Broker
Sets up the mqtt client to publish to a specific site queue and receive replyto messages.
Most of the input fields may be left alone.  Only the username and password for accessing
Rabbit are required to be filled in, plus you must specify a site id that is active to receive any data.








