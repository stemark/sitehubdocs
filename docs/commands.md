## Commands API

### Site Ops List
Request the list of commands that this sitehub can respond to.
`POST server/site_ops`

Confirmation:
```
POST server/site_ops

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}

```

Response
```json
POST server / site_ops

{
	"success": true,
	"results": {
		"data": [{
			"group": "Systems Control",
			"members": [{
				"name": "Reboot",
				"subject": "Sitehub",
				"target": "server",
				"action": "reboot_sitehub"
			}, {
				"name": "Reboot",
				"subject": "WAP",
				"target": "server",
				"action": "reboot_wap"
			}]
		}, {
			"group": "System Updates",
			"members": [{
				"name": "Update",
				"subject": "Core App",
				"target": "core_app",
				"action": "update"
			}]
		}, {
			"group": "Operations",
			"members": [{
				"name": "Logcat",
				"subject": "Sitehub",
				"target": "core_app",
				"action": "logcat"
			}]
		}]
	}
}
```
This device provides 4 commands that should be sectioned into 3 groups.  Name and subject are the descriptive labels for the action that will be performed and the target that will be affected.  The taget and action are the specific POST values that would be sent to trigger the command.


### Issuing a Command
Commands are issued by specifying the target/action in the POST line and then 

Request to save a snaphot of the log file:
```
replyto sitehub/40882
POST core_app/logcat

{"method":"GET","get":{"name":"Logcat","value":"Sitehub"}}
```
Confirmation response: 
```
POST core_app/logcat

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.coreops.DashOpsService"]}}
```

Request to reboot the sitehub:
```
replyto sitehub/40882
POST server/reboot_sitehub

{"method":"GET","get":{"name":"Reboot","value":"Sitehub"}}
```
Confirmation response:
```
POST server/reboot_sitehub

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

