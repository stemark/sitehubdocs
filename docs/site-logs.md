## Site Log Management API

### List snapshots and logfiles
List the retained log files generated periodically by the system.  Logs may be downloaded directly from the device at http://sitehub-ip:15080/logs/:logfile
```
POST server/site_logs
```

Resolution response
```json
POST server/site_logs

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.support.nanohfs.ContentDeliveryService"]}}
```

Example response
```json
POST server/site_logs

{"success":true,"results":{"data":[
  {"folder":"daily16",
   "files":[
    {"name":"20181016-014849.log","size":1500664,"date":"10\/16, 1:48 AM"},
    {"name":"20181016-034920.log","size":1742705,"date":"10\/16, 3:49 AM"},
    {"name":"20181016-074920.log","size":1883379,"date":"10\/16, 7:49 AM"},
    {"name":"20181016-114920.log","size":2070626,"date":"10\/16, 11:49 AM"},
    {"name":"20181016-124920.log","size":2246480,"date":"10\/16, 12:49 PM"}
    ]},
  {"folder":"daily15",
   "files":[
    {"name":"20181015-122440.log","size":4190636,"date":"10\/15, 12:24 PM"},
    {"name":"20181015-142650.log","size":4677954,"date":"10\/15, 2:26 PM"},
    {"name":"20181015-152650.log","size":504853,"date":"10\/15, 3:26 PM"},
    {"name":"20181015-192650.log","size":1371673,"date":"10\/15, 7:26 PM"},
    {"name":"20181015-232650.log","size":1401681,"date":"10\/15, 11:26 PM"}
    ]},
  {"folder":"daily13",
   "files":[
    {"name":"20181013-100947.log","size":2370525,"date":"10\/13, 10:09 AM"},
    {"name":"20181013-142341.log","size":2005253,"date":"10\/13, 2:23 PM"}
    ]},
  {"folder":"daily12",
   "files":[
    {"name":"20181012-135126.log","size":2633003,"date":"10\/12, 1:51 PM"},
    {"name":"20181012-135846.log","size":3208885,"date":"10\/12, 1:58 PM"},
    {"name":"20181012-143323.log","size":431478,"date":"10\/12, 2:33 PM"},
    {"name":"20181012-232838.log","size":3910472,"date":"10\/12, 11:28 PM"}
   ]}
 ]
}}
```

### Take a screenshot
Trigger action to capture a screen shot and save it in the logs folders

 name | value
 ------- | -------
 method | GET 
 get | name value pairs
 name | label of the action for display purposes 
 value | target device for the action (SiteHub)

Example Request
```
replyto sitehub/40882
POST core_app/screen_cap

{"method":"GET","get":{"name":"ScreenShot","value":"SiteHub"}}
```

Resolution response
```json
POST core_app/screen_cap

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.coreops.DashOpsService"]}}
```


### Take a Log snapshot
Capture the current logcat entries to a file in the logs folder
name | value
 ------- | -------
 method | GET 
 get | name value pairs
 name | label of the action for display purposes 
 value | target device for the action (SiteHub)

Example Request
```
replyto sitehub/40882
POST core_app/logcat

{"method":"GET","get":{"name":"Logcat","value":"SiteHub"}}
```

Resolution response
```json
POST core_app/logcat

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.coreops.DashOpsService"]}}
```
### Sync all logs via HTTP PUT
Sync each logfile and folder to the nexus log repository 
The upload credentials will use the SiteHub.Settings.log_url and SiteHub.Settings.log_url.auth with defaults set to https://nexus.buzztime.com/repository/sitehub/logs/{siteId}
and user/pwd set to ace_reporter/@nexus.librarian

name | value
 ------- | -------
 method | GET 
 get | name value pairs
 name | label of the action for display purposes 
 value | target device for the action (SiteHub)

Example Request
```
replyto sitehub/41098
POST dash_op/sync_logs

{"method":"GET","get":{"name":"NexusSync","value":"Sitehub"}}
```
resolution response 
```
POST dash_op/sync_logs

{"success":true,"results":{"type":"service","resolved_intents":["com.buzztime.core.dashops.DashOpsService"]}}
```

``` sync logs return list
POST dash_op/sync_logs

{"success":true,"results":{"data":[...]}}

### PUT a single logfile
This is currently only supported in the Dash App for locally connected browsers to directly download a single log file.

### DEV ONLY : rsync logs
This will most likely be deprecated soon.  It is an interim solution for locally connect devs to sync logs to a server with an rsync daemon running on it.
